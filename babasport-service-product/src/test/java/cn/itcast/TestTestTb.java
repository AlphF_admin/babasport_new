package cn.itcast;

import java.io.File;
import java.io.IOException;
import java.util.Date;

import cn.itcast.common.FastDfsUtils;
import cn.itcast.core.service.TestTbService;
import org.apache.commons.io.FileUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import cn.itcast.core.pojo.TestTb;

/**
 * 测试测试表
 * @author lx
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:application-context.xml"})
public class TestTestTb {

//	此处不能测试dubbo了
	@Autowired
//	private TestTbDao testTbDao;
	private TestTbService testTbService;
	
	@Test
	public void testname() throws Exception {
		
		TestTb testTb = new TestTb();
		testTb.setName("曾志伟2");
		testTb.setBirthday(new Date());
		testTbService.insertTestTb(testTb);
	}
	//测试上传下载：

	@Test
	public void testFastDFS() throws Exception {
		String path = "C:\\Users\\jiangfeng\\Pictures\\Saved Pictures\\ludashi.jpg";
		// 	读图片到内存中
		File file =new File(path);
		byte[] readFileToByteArray=null;
		try {
			 readFileToByteArray = FileUtils.readFileToByteArray(new File(path));
		} catch (IOException e) {
			e.printStackTrace();
		}
		path =FastDfsUtils.uploadPic(readFileToByteArray,"ludashi.jpg",file.getTotalSpace());
		System.out.println(path);
	}
}
