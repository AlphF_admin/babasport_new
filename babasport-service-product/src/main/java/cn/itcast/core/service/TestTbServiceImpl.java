package cn.itcast.core.service;

import cn.itcast.core.dao.TestTbDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import cn.itcast.core.pojo.TestTb;

/**
 * 测试事务
 */
@Service("testTbService")
@Transactional
@com.alibaba.dubbo.config.annotation.Service
public class TestTbServiceImpl implements TestTbService {

	@Autowired
	private TestTbDao testTbDao;
	@Override
	public void insertTestTb(TestTb testTb) {
		testTbDao.insertTestTb(testTb);
//		throw new RuntimeException();
	}
}
