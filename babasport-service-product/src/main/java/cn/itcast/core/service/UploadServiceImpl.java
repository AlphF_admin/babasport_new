package cn.itcast.core.service;

import cn.itcast.common.FastDfsUtils;
import cn.itcast.common.page.Pagination;
import cn.itcast.core.pojo.user.Addr;
import cn.itcast.core.dao.user.AddrDao;
import cn.itcast.core.pojo.user.AddrQuery;
import cn.itcast.core.service.UploadService;
import org.apache.commons.io.FileUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * 地址
 */
@Service("uploadService")
@Transactional
public class UploadServiceImpl implements UploadService {
	@Override
	public String upload(File file) {
		String path = null;
		try {
			path = FastDfsUtils.uploadPic(FileUtils.readFileToByteArray(file), file.getName(), file.getTotalSpace());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return path;
	}

	@Override
	public String upload(byte[] pic, String name, long size) {
		String path = null;
		try {
			path=  FastDfsUtils.uploadPic(pic,name,size);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return path;
	}


}
