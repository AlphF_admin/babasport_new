package cn.itcast.core.controller;

import cn.itcast.common.ResponseUtils;
import cn.itcast.core.service.UploadService;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import net.fckeditor.response.UploadResponse;
import org.apache.commons.io.FilenameUtils;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.Random;

/**
 * 身体 frame 控制器
 *
 */
@Controller
@RequestMapping(value = "")
public class UploadController {

	@Autowired
	UploadService uploadService;


	@RequestMapping(value = "/upload/uploadPic.do")
	public void uploadPic(@RequestParam(value = "pic",required = false)MultipartFile pic, String name, HttpServletResponse response){
		//扩展名
		String ext = FilenameUtils.getExtension(pic.getOriginalFilename());
		// todo 此处不能支持中文 获取文件名称，所以只能重命名了
		// String filename = pic.getOriginalFilename();
		String path = null;
		try {
			path =uploadService.upload(pic.getBytes(),pic.getOriginalFilename(),pic.getSize());
		} catch (IOException e) {
			e.printStackTrace();
		}

		String url = "http://192.168.72.130/"+path;

		// 转成json字符串，传到前台。
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("url", url);

		ResponseUtils.renderJson(response, jsonObject.toString());
	}


	@RequestMapping(value = "/upload/uploadPic2.do")
	// requeired = false, 表示该参数非必须。默认为true
	public void uploadPic2(@RequestParam(value = "pic",required = false)MultipartFile pic, String name, HttpServletResponse response){

		// 获取图片文件的第二种方式
		// String path = request.getSession().getServletContext().getRealPath("upload");
		// MultipartHttpServletRequest mreq = (MultipartHttpServletRequest)request;
		// MultipartFile file = mreq.getFile("pic");

		//扩展名
		String ext = FilenameUtils.getExtension(pic.getOriginalFilename());
		// todo 此处不能支持中文 获取文件名称，所以只能重命名了
		// String filename = pic.getOriginalFilename();

		String path = getpicName()+"."+ext;
		// 上传到另一台服务器
		// 实例化一个 Jersey
		Client client = new Client();
		// 另一台服务器的请求路径, 发送的目录
		String url = "http://localhost:8088/image-web/upload/"+path;
		// 设置请求路径
		WebResource resource = client.resource(url);

		try {
			// 发送开始， 通过put方式发送
			resource.put(String.class,pic.getBytes());
		} catch (IOException e) {
			e.printStackTrace();
		}

		// 转成json字符串，传到前台。
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("url", url);
		jsonObject.put("path", url);

		ResponseUtils.renderJson(response, jsonObject.toString());
	}

	/**
	 * 上传fck的图片： fck的图片名字不知道。
	 */
	@RequestMapping(value = "/upload/uploadFck.do")
	public void uploadFck(HttpServletRequest request, HttpServletResponse response){
		//强转request  支持多个
		MultipartHttpServletRequest mr= (MultipartHttpServletRequest)request;
		//获取值  支持多个
		Map<String, MultipartFile> fileMap = mr.getFileMap();
		//遍历Map
		for(Map.Entry<String, MultipartFile> entry : fileMap.entrySet()){
			//上传上来的图片
			MultipartFile pic = entry.getValue();
			//扩展名
			String ext = FilenameUtils.getExtension(pic.getOriginalFilename());

			//实例化一个Jersey
			Client client = new Client();
			//保存数据库
			String path = getpicName() + "." + ext;

			//另一台服务器的请求路径是?
			String url = "http://localhost:8088/image-web/upload/"  + path;
			//设置请求路径
			WebResource resource = client.resource(url);

			//发送开始  POST  GET   PUT
			try {
				resource.put(String.class, pic.getBytes());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			//返回Url给Fck   fck-core...jar   ckeditor
			UploadResponse ok = UploadResponse.getOK(url);
			//response 返回对象
			//response  write
			//response  print
			//区别:
			//字符流
			//字节流
			try {
				response.getWriter().print(ok);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

	}

	private String getpicName(){
		//图片名称生成策略
		DateFormat df = new SimpleDateFormat("yyyyMMddHHmmssSSS");
		//图片名称一部分
		String format = df.format(new Date());

		//随机三位数
		Random r = new Random();
		// n 1000   0-999   99
		for(int i=0 ; i<3 ;i++){
			format += r.nextInt(10);
		}
		return format;
	}


//	@InitBinder
//	public void initBinder(WebDataBinder dataBinder, WebRequest request){
//		DateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//		dataBinder.registerCustomEditor(Date.class,new CustomDateEditor(dateformat,true));
//	}


}
