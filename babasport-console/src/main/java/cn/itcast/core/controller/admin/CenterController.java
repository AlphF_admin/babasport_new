package cn.itcast.core.controller.admin;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import cn.itcast.core.pojo.TestTb;
import cn.itcast.core.service.TestTbService;

/**
 * 后台管理中心  Shift+Alt+R
 * @author lx
 *
 */
@Controller
@RequestMapping(value = "/control")
public class CenterController {

//	@com.alibaba.dubbo.config.annotation.Reference
	@Autowired
	private TestTbService testTbService;

	@RequestMapping(value = "/{actionName}.do")
	public String index(@PathVariable String actionName){
		return actionName;
	}

//	@RequestMapping(value = "/index.do")
//	public String index(){
//		return "index";
//	}


	//测试
	@RequestMapping(value = "/test/index.do")
	public String test(){
		TestTb testTb = new TestTb();
		testTb.setName("范冰冰");
		testTb.setBirthday(new Date());
		testTbService.insertTestTb(testTb);

		return "index";
	}

}
