package cn.itcast.core.controller.admin;

import cn.itcast.common.page.Pagination;
import cn.itcast.core.pojo.product.*;
import cn.itcast.core.service.product.*;
import cn.itcast.core.service.staticpage.StaticPageService;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 商品 控制器
 *
 */
@Controller
@RequestMapping(value = "/control")
public class ProductController {

	@Autowired
	BrandService brandService;
	@Autowired
	ProductService productService;
	@Autowired
	TypeService typeService;
	@Autowired
	FeatureService featureService;
	@Autowired
	ColorService colorService;
	@Autowired
	StaticPageService staticPageService;
	@Autowired
	SkuService skuService;

	@RequestMapping(value = "/product/list.do")
	public String product_list(Integer pageNo,String name,Integer brandId,Integer isShow,ModelMap model){
		//品牌条件对象
		BrandQuery brandQuery = new BrandQuery();
		//设置指定字段
		brandQuery.setFields("id,name");
		//设置可见
		brandQuery.setIsDisplay(1);
		//加载品牌
		List<Brand> brands = brandService.getBrandList(brandQuery);
		//显示在页面
		model.addAttribute("brands", brands);

		//分页参数
		StringBuilder params = new StringBuilder();
		//商品条件对象
		ProductQuery productQuery = new ProductQuery();
		//1:判断条件是为Null
		if(StringUtils.isNotBlank(name)){
			productQuery.setName(name);
			//要求模糊查询
			productQuery.setNameLike(true);

			params.append("&name=").append(name);

			//回显查询条件
			model.addAttribute("name", name);
		}
		//判断品牌ID
		if(null != brandId){
			productQuery.setBrandId(brandId);
			params.append("&").append("brandId=").append(brandId);

			//回显查询条件
			model.addAttribute("brandId", brandId);
		}
		//判断上下架状态
		if(null != isShow){
			productQuery.setIsShow(isShow);
			params.append("&").append("isShow=").append(isShow);

			//回显查询条件
			model.addAttribute("isShow", isShow);
		}else{
			productQuery.setIsShow(0);
			params.append("&").append("isShow=").append(0);
			//回显查询条件
			model.addAttribute("isShow", 0);
		}
		//设置页号
		productQuery.setPageNo(Pagination.cpn(pageNo));
		//设置每页数
		productQuery.setPageSize(5);
		//按照ID倒排
		productQuery.orderbyId(false);

		//加载带有分页的商品
		Pagination pagination = productService.getProductListWithPage(productQuery);

		//分页页面展示    //String params = "brandId=1&name=2014瑜伽服套装新款&pageNo=1";
		String url = "/product/list.do";
		pagination.pageView(url, params.toString());

		model.addAttribute("pagination", pagination);


		return "product/list";
	}

	@RequestMapping(value = "/product/toAdd.do")
	public String type_toAdd(ModelMap model){
		//加载商品类型
		TypeQuery typeQuery = new TypeQuery();
		//指定查询哪些字段
		typeQuery.setFields("id,name");
		typeQuery.setIsDisplay(1);
		typeQuery.setParentId(1);
		List<Type> types = typeService.getTypeList(typeQuery);
		//显示在页面
		model.addAttribute("types", types);
		//加载商品品牌
		//品牌条件对象
		BrandQuery brandQuery = new BrandQuery();
		//设置指定字段
		brandQuery.setFields("id,name");
		//设置可见
		brandQuery.setIsDisplay(1);
		//加载品牌
		List<Brand> brands = brandService.getBrandList(brandQuery);
		//显示在页面
		model.addAttribute("brands", brands);
		//加载商品属性
		FeatureQuery featureQuery = new FeatureQuery();

		List<Feature> features = featureService.getFeatureList(featureQuery);
		//显示在页面
		model.addAttribute("features", features);
		//加载颜色
		ColorQuery colorQuery = new ColorQuery();
		colorQuery.setParentId(0);
		List<Color> colors = colorService.getColorList(colorQuery);
		//显示在页面
		model.addAttribute("colors", colors);

		return "product/add";
	}

	// 新加商品
	@RequestMapping(value = "/product/add.do")
	public String add(ModelMap model, Product product, Img img){
		//1:商品 表   图片表   SKu表
		product.setImg(img);
		//传商品对象到Servcie
		productService.addProduct(product);
		return "redirect:list.do";
	}

	// 上架
	@RequestMapping(value = "/product/isShow.do")
	public String isShow(Integer[] ids,Integer pageNo,String name,Integer brandId,Integer isShow,ModelMap model){
		//实例化商品
		Product product = new Product();
		product.setIsShow(1);
		//上架
		if(null != ids && ids.length >0){
			for(Integer id : ids){
				product.setId(id);
				//修改上架状态
				productService.updateProductByKey(product);
				Map root = new HashMap();

				//DONE 静态化,用freeMarker，直接生成html
				//商品加载
				Product p = productService.getProductByKey(id);
				root.put("product", p);
				//skus,有库存的
				List<Sku> skus = skuService.getStock(id);
				// 这种对象只能给 大括号用，如果想给js用 就转成json
				//model.addAttribute("skus", skus);
				String jsonObject = JSONObject.valueToString(skus);
				root.put("skus", jsonObject);
				//去重复
				List<Color>  colors = new ArrayList<Color>();
				//遍历SKu
				for(Sku sku : skus){
					//判断集合中是否已经有此颜色对象了
					if(!colors.contains(sku.getColor())){
						colors.add(sku.getColor());
					}
				}
				root.put("colors", colors);
				// 返回静态页的访问地址
				String path = staticPageService.pageIndex(root,id);
			}
		}

		//判断
		if(null != pageNo){
			model.addAttribute("pageNo", pageNo);
		}
		if(StringUtils.isNotBlank(name)){
			model.addAttribute("name", name);
		}
		if(null != brandId){
			model.addAttribute("brandId", brandId);
		}
		if(null != isShow){
			model.addAttribute("isShow", isShow);
		}

		return "redirect:list.do";
	}


	@RequestMapping(value = "/type/list.do")
	public String type_list(){
		return "type/list";
	}

	//订单的身体
	@RequestMapping(value = "/order_main.do")
	public String orderMain(){

		return "frame/order_main";
	}
	//订单的左
	@RequestMapping(value = "/order_left.do")
	public String orderLeft(){

		return "frame/order_left";
	}

//	@InitBinder
//	public void initBinder(WebDataBinder dataBinder, WebRequest request){
//		DateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//		dataBinder.registerCustomEditor(Date.class,new CustomDateEditor(dateformat,true));
//	}


}
