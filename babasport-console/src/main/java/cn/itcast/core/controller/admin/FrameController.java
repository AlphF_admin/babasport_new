package cn.itcast.core.controller.admin;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 身体 frame 控制器
 *
 */
@Controller
@RequestMapping(value = "/control")
public class FrameController {
	//control/frame/product_main
	//todo 不知道为啥这么写不行！  要去掉$ 符号
	@RequestMapping(value = "/frame/{actionName}.do")
	public String index(@PathVariable String actionName){
		return "frame/"+actionName;
	}

	/*@RequestMapping(value = "/frame/product_main.do")
	public String product_main(){
		return "frame/product_main";
	}
	@RequestMapping(value = "/frame/product_left.do")
	public String product_left(){
		return "frame/product_left";
	}*/









//	@InitBinder
//	public void initBinder(WebDataBinder dataBinder, WebRequest request){
//		DateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//		dataBinder.registerCustomEditor(Date.class,new CustomDateEditor(dateformat,true));
//	}


}
