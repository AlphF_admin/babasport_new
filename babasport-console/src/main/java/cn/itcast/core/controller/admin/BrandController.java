package cn.itcast.core.controller.admin;

import cn.itcast.common.ResponseUtils;
import cn.itcast.common.page.Pagination;
import cn.itcast.core.pojo.product.Brand;
import cn.itcast.core.service.product.BrandService;
import com.alibaba.dubbo.config.annotation.Reference;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

/**
 * 品牌 控制器
 *
 */
@org.springframework.stereotype.Controller
@RequestMapping(value = "/control/brand/")
public class BrandController {

//	@Reference
	@Autowired
	BrandService brandService;

	@RequestMapping(value = "list.do")
	public String brand_list(String name,Integer isDisplay,Integer pageNo, ModelMap model){
		//参数
		//StringBuilder params = new StringBuilder();
		List<String> params =new ArrayList<String>();
		Brand brand = new Brand();
		//判断传进来的名称是否为Null并且还要判断 是否为空串   blank  ""  "   "   emtpy  ""   "  "
		if(StringUtils.isNotBlank(name)){
			brand.setName(name);
			params.add("name="+name);
		}
		//是  不是
		if(null != isDisplay){
			// mybaits 也有判空的处理，此处不用写了
			brand.setIsDisplay(isDisplay);
			params.add("isDisplay="+isDisplay);
		}else{
			brand.setIsDisplay(1);
			params.add("isDisplay="+1);
		}

		// 查询到分好页的数据
		Pagination pagination = brandService.getBrandListWithPage(brand, pageNo);
		pagination.pageView("list.do", listToString(params, '&'));

		model.addAttribute("pagination", pagination);
		// 设置回显
		model.addAttribute("name", name);
		model.addAttribute("isDisplay", isDisplay);
		return "brand/list";
	}

	@RequestMapping(value = "toadd.do")
	public String brand_toadd(){
		return "brand/add";
	}

	@RequestMapping(value = "edit.do")
	public String brand_edit(Integer brandId, ModelMap model){
		// 先查询一下
		Brand brand = brandService.findBrandByid(brandId);

		model.addAttribute("brand", brand);
		return "brand/edit";
	}

	@RequestMapping(value = "save.do")
	// todo 用final RedirectAttributes redirectAttributes 报错
	public String brand_save(Brand brand, ModelMap model){
		// 保存
		brandService.saveBrand(brand);
		model.addAttribute("brand", brand);
		model.addAttribute("message","修改成功!");

		return "brand/edit";
	}

	@RequestMapping(value = "add.do")
	public String brand_add(Brand brand){
		brandService.addBrand(brand);
		//重定向到品牌列表页面
		return "redirect:list.do";

	}

	@RequestMapping(value = "delete.do")
	public String brand_delete(Integer brandId){
		brandService.deleteBrand(brandId);

		//重定向到品牌列表页面
		return "redirect:list.do";

	}

	@RequestMapping(value = "batchDeleteBrand.do")
	public void brand_batchDeleteBrand(String brandIds, HttpServletResponse response){
		if(null == brandIds) return;
		brandService.batchDeleteBrand(brandIds);

		//重定向到品牌列表页面
		//return "redirect:list.do";
		ResponseUtils.renderJson(response, "删除成功！");
	}

	public String listToString(List list, char separator) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < list.size(); i++) {
			sb.append(list.get(i));
			if (i < list.size() - 1) {
				sb.append(separator);
			}
		}
		return sb.toString();
	}






//	@InitBinder
//	public void initBinder(WebDataBinder dataBinder, WebRequest request){
//		DateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//		dataBinder.registerCustomEditor(Date.class,new CustomDateEditor(dateformat,true));
//	}


}
