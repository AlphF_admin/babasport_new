package cn.itcast.core.dao.product;

import cn.itcast.core.pojo.product.Brand;
import cn.itcast.core.pojo.product.BrandQuery;

import java.util.List;
import java.util.Map;

//@Component 由mybatis自己扫描， 不让spring扫到
public interface BrandDao {

	// 根据id查品牌
	public Brand findBrandByid(Integer id);

	// 分页查询品牌
	public List<Brand> getBrandListWithPage(Map queryParams);

	//查询集合
	public List<Brand> getBrandList(BrandQuery brandQuery);

	// 查询总品牌数
	public Integer getBrandCount(Brand brand);

	// 添加品牌
	public void addBrand(Brand brand);

	// 删除品牌
	public void deleteBrand(Integer id);

	// 批量删除品牌
	public void batchDeleteBrand(String[] ids);

	// 编辑品牌
	public void saveBrand(Brand brand);

}
