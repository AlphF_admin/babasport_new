package cn.itcast.common.web;

import org.springframework.core.convert.converter.Converter;

/**
 * 去除空字符串
 *
 *  页面传入的类型
 *
 *T程序里面的类型
 */
public class CustomStringEdtor implements Converter<String,String> {


	@Override
	public String convert(String source) {
		if(null != source){
			source=source.trim();
			if(!"".equals(source)){
				return source;
			}
		}
		return null;
	}
}
