package cn.itcast.core.service.order;

import java.util.List;


import cn.itcast.core.pojo.order.Detail;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.itcast.common.page.Pagination;
import cn.itcast.core.pojo.order.DetailQuery;
/**
 * 订单子项(订单详情)
 * @author lixu
 * @Date [2014-3-27 下午03:31:57]
 */
@Service
public interface DetailService {


	/**
	 * 插入数据库
	 */
	public Integer addDetail(Detail detail);

	/**
	 * 根据主键查找
	 */
	@Transactional(readOnly = true)
	public Detail getDetailByKey(Integer id);

	@Transactional(readOnly = true)
	public List<Detail> getDetailsByKeys(List<Integer> idList);

	/**
	 * 根据主键删除
	 */
	public Integer deleteByKey(Integer id);

	public Integer deleteByKeys(List<Integer> idList);

	/**
	 * 根据主键更新
	 */
	public Integer updateDetailByKey(Detail detail);

	public Pagination getDetailListWithPage(DetailQuery detailQuery);

	public List<Detail> getDetailList(DetailQuery detailQuery);
}
