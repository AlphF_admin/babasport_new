package cn.itcast.core.service.product;

import java.util.List;


import cn.itcast.core.pojo.product.Feature;
import org.springframework.stereotype.Service;

import cn.itcast.common.page.Pagination;
import cn.itcast.core.pojo.product.FeatureQuery;
/**
 * 商品属性事务层
 * @author lixu
 * @Date [2014-3-27 下午03:31:57]
 */
@Service
public interface FeatureService  {

	/**
	 * 插入数据库
	 */
	public Integer addFeature(Feature feature);

	/**
	 * 根据主键查找
	 */
	public Feature getFeatureByKey(Integer id);
	
	public List<Feature> getFeaturesByKeys(List<Integer> idList);

	/**
	 * 根据主键删除
	 */
	public Integer deleteByKey(Integer id);

	public Integer deleteByKeys(List<Integer> idList);

	/**
	 * 根据主键更新
	 */
	public Integer updateFeatureByKey(Feature feature);

	public Pagination getFeatureListWithPage(FeatureQuery featureQuery) ;

	public List<Feature> getFeatureList(FeatureQuery featureQuery);
}
