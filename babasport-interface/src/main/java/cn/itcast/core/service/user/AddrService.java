package cn.itcast.core.service.user;

import java.util.List;

import cn.itcast.core.pojo.user.Addr;
import cn.itcast.core.pojo.user.AddrQuery;
import org.springframework.transaction.annotation.Transactional;

import cn.itcast.common.page.Pagination;

/**
 * 地址
 * @author lixu
 * @Date [2014-3-27 下午03:31:57]
 */
//@Service 交给dubbo了
@Transactional
public interface AddrService {

	/**
	 * 插入数据库
	 * 
	 * @return
	 */
	public Integer addAddr(Addr addr);

	/**
	 * 根据主键查找
	 */
	@Transactional(readOnly = true)
	public Addr getAddrByKey(Integer id);
	
	@Transactional(readOnly = true)
	public List<Addr> getAddrsByKeys(List<Integer> idList);

	/**
	 * 根据主键删除
	 * 
	 * @return
	 */
	public Integer deleteByKey(Integer id);

	public Integer deleteByKeys(List<Integer> idList);

	/**
	 * 根据主键更新
	 * 
	 * @return
	 */
	public Integer updateAddrByKey(Addr addr);
	
	@Transactional(readOnly = true)
	public Pagination getAddrListWithPage(AddrQuery addrQuery);
	
	@Transactional(readOnly = true)
	public List<Addr> getAddrList(AddrQuery addrQuery);
}
