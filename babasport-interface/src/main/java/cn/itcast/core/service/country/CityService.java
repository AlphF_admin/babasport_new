package cn.itcast.core.service.country;

import java.util.List;


import cn.itcast.core.pojo.country.City;
import org.springframework.stereotype.Service;

import cn.itcast.common.page.Pagination;
import cn.itcast.core.pojo.country.CityQuery;
/**
 * 市
 * @author lixu
 * @Date [2014-3-27 下午03:31:57]
 */
@Service
public interface CityService {


	/**
	 * 插入数据库
	 * 
	 * @return
	 */
	public Integer addCity(City city);

	/**
	 * 根据主键查找
	 */
	public City getCityByKey(Integer id);
	
	public List<City> getCitysByKeys(List<Integer> idList);

	/**
	 * 根据主键删除
	 * 
	 */
	public Integer deleteByKey(Integer id);

	public Integer deleteByKeys(List<Integer> idList);

	/**
	 * 根据主键更新
	 * 
	 * @return
	 */
	public Integer updateCityByKey(City city);
	
	public Pagination getCityListWithPage(CityQuery cityQuery) ;
	
	public List<City> getCityList(CityQuery cityQuery);
}
