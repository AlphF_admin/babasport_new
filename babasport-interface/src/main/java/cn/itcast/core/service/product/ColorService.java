package cn.itcast.core.service.product;

import java.util.List;


import org.springframework.stereotype.Service;

import cn.itcast.common.page.Pagination;
import cn.itcast.core.pojo.product.Color;
import cn.itcast.core.pojo.product.ColorQuery;
/**
 * 颜色
 * @author lixu
 * @Date [2014-3-27 下午03:31:57]
 */
@Service
public interface ColorService {

	/**
	 * 插入数据库
	 */
	public Integer addColor(Color color);

	/**
	 * 根据主键查找
	 */
	public Color getColorByKey(Integer id);
	
	public List<Color> getColorsByKeys(List<Integer> idList);

	/**
	 * 根据主键删除
	 * 
	 */
	public Integer deleteByKey(Integer id);

	public Integer deleteByKeys(List<Integer> idList);

	/**
	 * 根据主键更新
	 * 
	 */
	public Integer updateColorByKey(Color color);
	
	public Pagination getColorListWithPage(ColorQuery colorQuery) ;
	
	public List<Color> getColorList(ColorQuery colorQuery);
}
