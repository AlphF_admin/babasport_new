package cn.itcast.core.service.country;

import java.util.List;


import org.springframework.stereotype.Service;

import cn.itcast.common.page.Pagination;
import cn.itcast.core.pojo.country.Town;
import cn.itcast.core.pojo.country.TownQuery;
/**
 * 县/区
 * @author lixu
 * @Date [2014-3-27 下午03:31:57]
 */
@Service
public interface TownService  {


	/**
	 * 插入数据库
	 * 
	 */
	public Integer addTown(Town town);

	/**
	 * 根据主键查找
	 */
	public Town getTownByKey(Integer id);
	
	public List<Town> getTownsByKeys(List<Integer> idList);

	/**
	 * 根据主键删除
	 * 
	 */
	public Integer deleteByKey(Integer id);

	public Integer deleteByKeys(List<Integer> idList);

	/**
	 * 根据主键更新
	 * 
	 * @return
	 */
	public Integer updateTownByKey(Town town);
	
	public Pagination getTownListWithPage(TownQuery townQuery) ;
	
	public List<Town> getTownList(TownQuery townQuery);
}
