package cn.itcast.core.service.staticpage;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.springframework.web.context.ServletContextAware;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer;

import javax.servlet.ServletContext;
import java.io.*;
import java.util.Map;


// 不用注解，用配置xml的方式注入
public interface StaticPageService {

	public String pageIndex(Map<String,Object> root,Integer id);
}
