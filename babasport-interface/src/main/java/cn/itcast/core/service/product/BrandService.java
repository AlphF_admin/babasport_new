package cn.itcast.core.service.product;

import cn.itcast.common.page.Pagination;
import cn.itcast.core.pojo.product.Brand;
import cn.itcast.core.pojo.product.BrandQuery;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public 	interface BrandService  {

	public Brand findBrandByid(Integer id);

	public List<Brand> getBrandList(BrandQuery brandQuery);

	public Pagination getBrandListWithPage(Brand brand, Integer pageNo);

	public void addBrand(Brand brand);

	public void deleteBrand(Integer id);

	public void batchDeleteBrand(String ids);

	public void saveBrand(Brand brand);
}
