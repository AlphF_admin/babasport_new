package cn.itcast.core.service;

import cn.itcast.core.pojo.TestTb;

import java.io.File;

public interface UploadService {
	
	//保存数据
	public String upload(File file);

	public String upload(byte[] pic,String name,long size);
}
