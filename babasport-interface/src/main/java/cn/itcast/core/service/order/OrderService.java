package cn.itcast.core.service.order;

import java.util.List;


import org.springframework.stereotype.Service;

import cn.itcast.common.page.Pagination;
import cn.itcast.core.pojo.order.Order;
import cn.itcast.core.pojo.order.OrderQuery;
/**
 * 订单主
 * @author lixu
 * @Date [2014-3-27 下午03:31:57]
 */
@Service
public interface OrderService  {


	/**
	 * 插入数据库
	 */
	public Integer addOrder(Order order);

	/**
	 * 根据主键查找
	 */
	public Order getOrderByKey(Integer id);
	
	public List<Order> getOrdersByKeys(List<Integer> idList);

	/**
	 * 根据主键删除
	 * 
	 */
	public Integer deleteByKey(Integer id);

	public Integer deleteByKeys(List<Integer> idList);

	/**
	 * 根据主键更新
	 * 
	 */
	public Integer updateOrderByKey(Order order);
	
	public Pagination getOrderListWithPage(OrderQuery orderQuery) ;
	
	public List<Order> getOrderList(OrderQuery orderQuery);
}
