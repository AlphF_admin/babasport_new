package cn.itcast.core.service.product;

import java.util.List;


import cn.itcast.core.pojo.product.Type;
import org.springframework.stereotype.Service;

import cn.itcast.common.page.Pagination;
import cn.itcast.core.pojo.product.TypeQuery;
/**
 * 商品类型
 * @author lixu
 * @Date [2014-3-27 下午03:31:57]
 */
@Service
public interface TypeService {


	/**
	 * 插入数据库
	 * 
	 * @return
	 */
	public Integer addType(Type type);

	/**
	 * 根据主键查找
	 */
	public Type getTypeByKey(Integer id);
	
	public List<Type> getTypesByKeys(List<Integer> idList);

	/**
	 * 根据主键删除
	 * 
	 */
	public Integer deleteByKey(Integer id);

	public Integer deleteByKeys(List<Integer> idList);

	/**
	 * 根据主键更新
	 * 
	 */
	public Integer updateTypeByKey(Type type);
	
	public Pagination getTypeListWithPage(TypeQuery typeQuery) ;
	
	public List<Type> getTypeList(TypeQuery typeQuery);
}
