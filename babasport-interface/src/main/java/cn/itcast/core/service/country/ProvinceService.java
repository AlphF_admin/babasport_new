package cn.itcast.core.service.country;

import java.util.List;


import org.springframework.stereotype.Service;

import cn.itcast.common.page.Pagination;
import cn.itcast.core.pojo.country.Province;
import cn.itcast.core.pojo.country.ProvinceQuery;
/**
 * 省
 * @author lixu
 * @Date [2014-3-27 下午03:31:57]
 */
@Service
public interface ProvinceService  {


	/**
	 * 插入数据库
	 */
	public Integer addProvince(Province province);

	/**
	 * 根据主键查找
	 */
	public Province getProvinceByKey(Integer id);
	
	public List<Province> getProvincesByKeys(List<Integer> idList);

	/**
	 * 根据主键删除
	 * 
	 */
	public Integer deleteByKey(Integer id);

	public Integer deleteByKeys(List<Integer> idList);

	/**
	 * 根据主键更新
	 * 
	 */
	public Integer updateProvinceByKey(Province province);

	public Pagination getProvinceListWithPage(ProvinceQuery provinceQuery);
	
	public List<Province> getProvinceList(ProvinceQuery provinceQuery);
}
