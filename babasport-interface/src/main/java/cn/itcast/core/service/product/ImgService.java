package cn.itcast.core.service.product;

import java.util.List;


import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.itcast.common.page.Pagination;
import cn.itcast.core.pojo.product.Img;
import cn.itcast.core.pojo.product.ImgQuery;
/**
 * 
 * @author lixu
 * @Date [2014-3-27 下午03:31:57]
 */
@Service
public interface ImgService  {

	/**
	 * 插入数据库
	 */
	public Integer addImg(Img img);

	/**
	 * 根据主键查找
	 */
	public Img getImgByKey(Integer id);
	
	@Transactional(readOnly = true)
	public List<Img> getImgsByKeys(List<Integer> idList);

	/**
	 * 根据主键删除
	 * 
	 * @return
	 */
	public Integer deleteByKey(Integer id);

	public Integer deleteByKeys(List<Integer> idList);

	/**
	 * 根据主键更新
	 * 
	 */
	public Integer updateImgByKey(Img img);
	
	public Pagination getImgListWithPage(ImgQuery imgQuery) ;
	
	public List<Img> getImgList(ImgQuery imgQuery);
}
