package cn.itcast.core.service.product;

import java.util.List;


import cn.itcast.core.pojo.product.Product;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.itcast.common.page.Pagination;
import cn.itcast.core.pojo.product.ProductQuery;
/**
 * 商品事务层
 * @author lixu
 * @Date [2014-3-27 下午03:31:57]
 */
@Service
public interface ProductService {

	/**
	 * 插入数据库
	 * 
	 */
	public Integer addProduct(Product product);


	/**
	 * 根据主键查找
	 */
	public Product getProductByKey(Integer id);
	
	public List<Product> getProductsByKeys(List<Integer> idList);

	/**
	 * 根据主键删除
	 * 
	 */
	public Integer deleteByKey(Integer id);

	public Integer deleteByKeys(List<Integer> idList);

	/**
	 * 根据主键更新
	 * 
	 * @return
	 */
	public Integer updateProductByKey(Product product);

	public Pagination getProductListWithPage(ProductQuery productQuery);
	
	public List<Product> getProductList(ProductQuery productQuery);
}
