package cn.itcast.core.service.user;

import java.util.List;


import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.itcast.common.page.Pagination;
import cn.itcast.core.pojo.user.Employee;
import cn.itcast.core.pojo.user.EmployeeQuery;
/**
 * 员工
 * @author lixu
 * @Date [2014-3-27 下午03:31:57]
 */
@Service
public interface EmployeeService {

	/**
	 * 插入数据库
	 * 
	 */
	public Integer addEmployee(Employee employee);

	/**
	 * 根据主键查找
	 */
	@Transactional(readOnly = true)
	public Employee getEmployeeByKey(String id);
	
	@Transactional(readOnly = true)
	public List<Employee> getEmployeesByKeys(List<String> idList);

	/**
	 * 根据主键删除
	 */
	public Integer deleteByKey(String id);

	public Integer deleteByKeys(List<String> idList);

	/**
	 * 根据主键更新
	 */
	public Integer updateEmployeeByKey(Employee employee);


	public Pagination getEmployeeListWithPage(EmployeeQuery employeeQuery);
	
	public List<Employee> getEmployeeList(EmployeeQuery employeeQuery);
}
