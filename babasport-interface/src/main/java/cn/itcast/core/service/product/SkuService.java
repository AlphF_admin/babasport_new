package cn.itcast.core.service.product;

import java.util.List;


import org.springframework.stereotype.Service;

import cn.itcast.common.page.Pagination;
import cn.itcast.core.pojo.product.Sku;
import cn.itcast.core.pojo.product.SkuQuery;
/**
 * 最小销售单元事务层
 * @author lixu
 * @Date [2014-3-27 下午03:31:57]
 */
@Service
public interface SkuService {


	/**
	 * 插入数据库
	 */
	public Integer addSku(Sku sku);

	/**
	 * 根据主键查找
	 */
	public Sku getSkuByKey(Integer id);
	
	public List<Sku> getSkusByKeys(List<Integer> idList);

	/**
	 * 根据主键删除
	 * 
	 */
	public Integer deleteByKey(Integer id);

	public Integer deleteByKeys(List<Integer> idList);

	/**
	 * 根据主键更新
	 * 
	 */
	public Integer updateSkuByKey(Sku sku);
	
	public Pagination getSkuListWithPage(SkuQuery skuQuery) ;
	
	public List<Sku> getSkuList(SkuQuery skuQuery);

	// 根据商品id获取
	public List<Sku> getStock(Integer productId);

}
