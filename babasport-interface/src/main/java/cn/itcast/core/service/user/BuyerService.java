package cn.itcast.core.service.user;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.itcast.common.page.Pagination;
import cn.itcast.core.pojo.user.Buyer;
import cn.itcast.core.pojo.user.BuyerQuery;
/**
 * 购买者
 * @author lixu
 * @Date [2014-3-27 下午03:31:57]
 */
@Service
@Transactional
public interface BuyerService {

	/**
	 * 插入数据库
	 *
	 * @return
	 */
	public Integer addBuyer(Buyer buyer);

	/**
	 * 根据主键查找
	 */
	public Buyer getBuyerByKey(String id);

	public List<Buyer> getBuyersByKeys(List<String> idList);

	/**
	 * 根据主键删除
	 */
	public Integer deleteByKey(String id);

	public Integer deleteByKeys(List<String> idList);

	/**
	 * 根据主键更新
	 */

	@Transactional(readOnly = true)
	public Pagination getBuyerListWithPage(BuyerQuery buyerQuery);


	public List<Buyer> getBuyerList(BuyerQuery buyerQuery);
}